﻿#include <iostream>
#include <iomanip>

using namespace std;

int main(int argc, char* argv[])
{
    const unsigned int a1 = 3;
    const unsigned int a2 = 5;

    double A[] = { 1, 2, 3, 4, 5, 6, 7 };
    double Sum;

    cout << "Original: [ ";
    for (int i = 0; i < 7; ++i) {
        cout << A[i] << " ";
    }
    cout << "]" << endl;

    Sum = 0;

    for (int i = 0; i < 7; i++)
        Sum = Sum + A[i];

    cout << "Sum: " << Sum << endl;
    cout << "Middle: " << Sum / 7 << "\n" << endl;

    int ary[a1][a2] = {
        { 1, 2, 3, 4, 5 },
        { 2, 4, 6, 8, 10 },
        { 3, 6, 9, 12, 15 }
    };

    cout << "Original: " << endl;

    for (int i = 0; i < a1; i++) {
        for (int j = 0; j < a2; j++) {
            cout << setw(4) << ary[i][j];
        }
        cout << endl;
    }

    for (int i = 0; i < a1; i++)
    {
        for (int j = 0; j < a2; j++)
        {
            Sum += ary[i][j];
        }
    }

    cout << "Sum: " << Sum << endl;
    cout << "Middle: " << Sum / 15 << "\n" << endl;

    return 0;
}
